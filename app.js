angular.module('ionicApp', ['ionic'])
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('Login', {   //login page
                url : '/Login',
                templateUrl : 'Login.html',
                controller : 'EntryPageController'
            })
            //Regiseteration
            .state('main.Addpatient', {
                 url: '/Addpatient',
                 views: {
                     'main': {
                         templateUrl: 'Addpatient.html',
                         controller : 'AddpatientController'
                     }
                 }
            })
            
            
	        .state('main.Register', {			//Registarion page
                url: '/Register',
                views: {
                    'main': {
                        templateUrl: 'Register.html',
                        controller : 'HomePageController'
                    }
                }
            })
            .state('main', {			
                url : '/main',
                templateUrl : 'mainContainer.html',
                abstract : true,
                controller : 'MainController'
            })
        $urlRouterProvider.otherwise('/Login');
    }])


    .controller('MainController', [ '$scope', function($scope) {
        $scope.toggleMenu = function() {
            $scope.sideMenuController.toggleLeft();
        }
    }])

    .controller('EntryPageController', [ '$scope', '$state', function($scope, $state) {
        $scope.navTitle = 'Login ';

        $scope.signIn = function() {
            $state.go('main.home');
        }
    }])

    .controller('HomePageController', [ '$scope', '$state', function($scope, $state) {
        $scope.navTitle = 'Registeration';

        $scope.leftButtons = [{
            type: 'button-icon icon ion-navicon',
            tap: function(e) {
                $scope.toggleMenu();
            }
        }];
    }])

    .controller('InfoPageController', [ '$scope', '$state', function($scope, $state) {
        $scope.navTitle = 'Terms and Condition';

        $scope.leftButtons = [{
            type: 'button-icon icon ion-navicon',
            tap: function(e) {
                $scope.toggleMenu();
            }
        }];
    }])

    .controller('AddpatientController', [ '$scope', '$state', function($scope, $state) {
        $scope.navTitle = 'Addpatient';

        $scope.leftButtons = [{
            type: 'button-icon icon ion-navicon',
            tap: function(e) {
                $scope.toggleMenu();
            }
        }];
    }])



$(document).ready(function() {
  return $('.login').click(function(event) {
    if (false) {

    } else {
      return $('.state').html('<br><i class="fa fa-ban"></i><br><h2>Error</h2>The email or password you entered is incorrect, please try again.').css({
        color: '#eb5638'
      });
    }
  });
});




